/***********************************************************************************************************
Copyright (C) 2013 NEPS - Soluciones Informáticas
 
Este programa es software libre: usted puede redistribuirlo y/o modificarlo
bajo los términos de la Licencia Pública General GNU publicada
por la Fundación para el Software Libre, ya sea la versión 3
de la Licencia, o (a su elección) cualquier versión posterior.
 
Este programa se distribuye con la esperanza de que sea útil, pero
SIN GARANTÍA ALGUNA; ni siquiera la garantía implícita
MERCANTIL o de APTITUD PARA UN PROPÓSITO DETERMINADO.
Consulte los detalles de la Licencia Pública General GNU para obtener
una información más detallada.
 
Debería haber recibido una copia de la Licencia Pública General GNU
junto a este programa.
En caso contrario, consulte http://www.gnu.org/licenses/gpl-3.0.html
**********************************************************************************************************/
 
function ajax(type, url, jsonData, msgAjax, msgProgreso, mostrarBloqueo, functionSuccess, functionError, async) {
	async || (async = true);
    if(mostrarBloqueo){
		mostrarBloqueoAjax(msgAjax, msgProgreso);
	}

    $.ajax({
        type: type,
        url: url,
        async: async,
        data: jsonData,
		//accepts: 'application/json; charset=utf-8',
		dataType: 'json',
        success: function (retorno) {
			if(mostrarBloqueo){
				ocultarBloqueoAjax();
			}
            	          
			if (retorno.Success) {
				/* Todo bien */
				functionSuccess(retorno.Objeto);
			}
			else { 
				/* Error */				
				var msj = "Se generó un error: " + retorno.MensajeError;
				
				if (functionError == null || functionError == 'undefined') {
					alert(msj);
				} else {
					functionError(msj);
				}
			}             
        },
        error: function (objeto, title, mensaje) {
			if(mostrarBloqueo){
				ocultarBloqueoAjax();
			}
            
			if (functionError == null || functionError == 'undefined') {
				alert(title + ": " + mensaje + "\n\nDetalle error:\n  - Status: " + objeto.status + "\n  - Descripción: " + objeto.statusText);
			} else {
				functionError(title + ": " + mensaje + "\n\nDetalle error:\n  - Status: " + objeto.status + "\n  - Descripción: " + objeto.statusText);
			}
        }
    });
}
 
function mostrarBloqueoAjax(msgAjax, msgProgreso) {
    $.blockUI({ 
		message: '<h1>' + msgProgreso + '</h1>',
		css: { backgroundColor: 'rgba(0, 0, 0, .5)', color: '#c5c5c5', borderRadius: '9px', border: 'none !important', height: '70px', padding: '40px 0 0 0', fontSize: '9px', left: '33%', width: '35%'}
	});
}
 
function ocultarBloqueoAjax() {
    $.unblockUI();
}