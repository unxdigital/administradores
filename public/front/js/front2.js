$(document).ready(function() {
    var $grupoRazonSocial = $('#grupo_razon_social');
    var $grupoNombre = $('#grupo_apellido_nombre');
    var $razonSocial = $('#razon_social');
    var $nombre = $('#nombre');
    var $apellido = $('#apellido');
    var $cuit = $('#cuit');
    var $cuitConsorcio = $('#cuit_consorcio');
    var $matricula = $('#matricula');
    var $tipoPersona = $('#tipo_persona');
    var $calle = $('#calle');
    var $altura = $('#altura');

    function verMas(administrador) {
        var table = $("#tableAdministradores").DataTable();
        var datos = table.row(administrador).data();
        $("#lbl_razon_social").text(datos.RAZONSOCIAL);

        var domicilioConstituido = datos.CALLEADMINISTRADOR + ' ' +
            datos.ALTURAADMINISTRADOR + ' - ' +
            datos.PISOADMINISTRADOR + ' - ' +
            datos.DEPARTAMENTOADMINISTRADOR + ' - ' +
            datos.CPADMINISTRADOR;

        $("#lbl_domicilio_constituido").text(domicilioConstituido);
        $("#lbl_cuit").text(datos.CUIT);
        $("#lbl_cantidad_consorsios").text(datos.CANTIDADCONSORCIOS);
        $("#lbl_matricula").text(datos.MATRICULAID);
        $("#lbl_oneroso").text(datos.ONEROSO);
        $("#lbl_estado_matricula").text(datos.ESTADOMATRICULADESC);
        $("#lbl_fecha_alta").text(moment(datos.FECHAALTA).format("DD/MM/yyyy"));
        $("#lbl_tiene_sanciones").text(datos.TIENESANCIONES);
        $("#lbl_fecha_suspencion").text(moment(datos.FECHASUSPENSION).format("DD/MM/yyyy") == "Invalid date" ? "" : moment(datos.FECHASUSPENSION).format("DD/MM/yyyy"));

        $('#modalMensaje').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#modalMensaje').modal('show');
    }

    $(document).on('change', '#tipo_persona', function () {
        if(this.value === '1') {
            $grupoRazonSocial.addClass('d-none');
            $grupoNombre.removeClass('d-none');
            $razonSocial.val('');
        } else if(this.value === '2') {
            $grupoNombre.addClass('d-none');
            $grupoRazonSocial.removeClass('d-none');
            $nombre.val('');
            $apellido.val('');
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

    autoGuionCUIT($cuit);
    autoGuionCUIT($cuitConsorcio);

    $('#formulario').validate({
        lang: 'es',
        rules: {
            'matricula': {
                number: true
            },
            'cuit': {
                cuit: true
            },
            'cuit_consorcio': {
                cuit: true
            },
            'altura': {
                number: true
            }
        },
        submitHandler: function(form) {
            // your ajax would go here
            return false; // blocks regular submit since you have ajax
        }
    });

    $(document).on('click', '#buscar', function () {
        $('form').valid();
        buscar(false);
    });

    $("#tableAdministradores").DataTable({
        "paging": false,
        "info": false,
        "destroy": true,
        "searching": false,
        "lengthChange": false,
        "processing": false,
        "data": [],
        "responsive": true,
        "columns": [
            {
                "data": "CUITCONSORCIO",
                "searchable": false,
                "visible": true,
                defaultContent: ""
            },
            {
                "searchable": false,
                "visible": true,
                defaultContent: "",
                width: 150,
                className: "text-center",
                data: null,
                sortable: false,
                render: function(data, type, row, meta) {
                    var res = row.CALLE + ', ' + row.ALTURA;
                    return res;
                }
            },
            {
                "data": "RAZONSOCIAL",
                "searchable": false,
                "visible": true,
                defaultContent: ""
            },
            {
                "data": "CUIT",
                "searchable": false,
                "visible": true,
                defaultContent: ""
            },
            {
                "data": "MATRICULAID",
                "searchable": false,
                "visible": true,
                defaultContent: ""
            },
            {
                "searchable": false,
                "visible": true,
                defaultContent: "",
                width: 150,
                className: "text-center",
                data: null,
                sortable: false,
                render: function(data, type, row, meta) {
                    var res = '<button type="button" id="detalle" class="btn btn-secondary btn-sm" onclick="verMas(' + meta.row + ');"></i>Detalle</button>';
                    return res;
                }
            }
        ],
    });

    $("#tableAdministradores_wrapper").css('display', 'block');


    function autoGuionCUIT(txtBox) {
        txtBox.change(function () {
            completarTxtCUIT($(this));
        });

        txtBox.keyup(function () {
            completarTxtCUIT($(this));
        });
    }

    function completarTxtCUIT(imp) {
        var txt = imp.val().replace(/[^0-9]/g, '');
        var res = '';
        for (i in txt) {
            if (i == 2 || i == 10) {
                res += '-';
            }
            else if (i > 10) {
                break;
            }
            res += txt[i];
        }

        imp.val(res);
    }

    function buscar(porUrl) {

        if (!validarBusqueda())
            return;

        $("#tableAdministradores").dataTable().fnClearTable();

        var dataBuscar = {
            'cuit': $cuit.val().split('-').join(''),
            'matricula': $matricula.val(),
            'tipoPersona': $tipoPersona.val(),
            'razonSocial': $razonSocial.val(),
            'nombre': $nombre.val(),
            'apellido': $apellido.val(),
            'calle': $calle.val(),
            'altura': $altura.val(),
            'cuitConsorcio': $cuitConsorcio.val().split('-').join('')
        };

        ajax('POST', '/administradores', dataBuscar, 'Espere por favor...', 'Cargando Administradores...', true, function (retorno) {
            if (retorno != null && retorno.length > 0) {
                if (retorno.length > 500) {
                    cambiarAlert("info", "Por favor aplique mas filtros", "La búsqueda arrojó demasiados resultados.");
                    $("#alert").show();
                    return;
                }
                if (retorno != null && retorno != undefined) {
                    var table = $("#tableAdministradores").DataTable();
                    table.clear().draw();
                    table.rows.add(retorno).draw();

                    if (porUrl)
                        verMas(table.rows[0]);

                    mostrarMensajes(retorno[0]);
                }
            } else {
                cambiarAlert("info", "Por favor aplique mas filtros", "No hay coincidencias.");
                $("#alert").show();
                return;
            }

        }, function (mensaje) {
            //funcion para el error, puede ser optativo
            cambiarAlert("error", "Error", "Ocurrio un error.");
            $("#alert").show();
        }, false);
    }

    function mostrarMensajes(retorno) {
        $("#infoMensaje1").hide();
        $("#infoMensaje2").hide();

        if (retorno.MENSAJE1 != undefined || retorno.MENSAJE1 != '') {
            $("#infoMensaje1").show();
            $("#titulo_alert_info_mensaje1").html("Mensaje Informativo 1");
            $("#cuerpo_alert_info_mensaje1").html(retorno.MENSAJE1);
        }
        if (retorno.MENSAJE2 != undefined || retorno.MENSAJE2 != '') {
            $("#infoMensaje2").show();
            $("#titulo_alert_info_mensaje2").html("Mensaje Informativo 2");
            $("#cuerpo_alert_info_mensaje2").html(retorno.MENSAJE2);
        }
    }

    function validarBusqueda() {
        if ($("#cuit").val() == '' && $("#matricula").val() == '' && $("#tipo_persona").val() == '0' &&
            $("#razon_social").val() == '' && $("#nombre").val() == '' && $("#apellido").val() == '' && $("#calle").val() == '' &&
            $("#altura").val() == '' && $("#cuit_consorcio").val('')) {
            cambiarAlert("info", "Por favor aplique mas filtros", "La búsqueda arrojó demasiados resultados.");
            $("#alert").show();
            return false;
        }

        if ($("#tipo_persona").val() == '0' && ($("#razon_social").val() != '' || $("#nombre").val() != '' || $("#apellido").val() != '')) {
            cambiarAlert("info", "Por favor aplique mas filtros", "Debe seleccionar un tipo de persona");
            $("#alert").show();
            return false;
        }

        if ($("#tipo_persona").val() != '0' && $("#razon_social").val() == '' && $("#nombre").val() == '' && $("#apellido").val() == '') {
            cambiarAlert("info", "Por favor aplique mas filtros", "Debe ingresar un valor para razón social o apellido y nombre");
            $("#alert").show();
            return false;
        }

        if ($("#tipo_persona").val() == '1' && ($("#nombre").val() == '' || $("#apellido").val() == '')) {
            cambiarAlert("info", "Por favor aplique mas filtros", "Debe ingresar un valor para apellido y nombre");
            $("#alert").show();
            return false;
        }

        if ($("#tipo_persona").val() == '2' && $("#razon_social").val() == '') {
            cambiarAlert("info", "Por favor aplique mas filtros", "Debe ingresar un valor para razón social");
            $("#alert").show();
            return false;
        }

        if (($("#calle").val() != '' && $("#altura").val() == '') || ($("#calle").val() == '' && $("#altura").val() != '')) {
            cambiarAlert("info", "Por favor aplique mas filtros", "Debe ingresar calle y altura");
            $("#alert").show();
            return false;
        }
        return true;
    }

    function cambiarAlert(tipo, titulo, mensaje) {
        if (tipo == "error") {

            $("#titulo_alert").html(titulo);
            $("#cuerpo_alert").html(mensaje);
            $("#icon-alert").removeClass("fa-info-circle");
            $("#icon-alert").addClass("fa-times-circle");

            $("#alert").removeClass("alert-info");
            $("#alert").addClass("alert-danger");

            $("#btnAlert").removeClass("btn-info");
            $("#btnAlert").addClass("btn-danger");

        } else {

            $("#titulo_alert").html(titulo);
            $("#cuerpo_alert").html(mensaje);

            $("#icon-alert").removeClass("fa-times-circle");
            $("#icon-alert").addClass("fa-info-circle");

            $("#alert").removeClass("alert-danger");
            $("#alert").addClass("alert-info");

            $("#btnAlert").removeClass("btn-danger");
            $("#btnAlert").addClass("btn-info");
        }

        window.setTimeout(function () {
            $("#alert").fadeOut(300)
        }, 5000);

    }

    function ajax(type, url, jsonData, msgAjax, msgProgreso, mostrarBloqueo, functionSuccess, functionError, async) {
        async || (async = true);
        if(mostrarBloqueo){
            mostrarBloqueoAjax(msgAjax, msgProgreso);
        }

        $.ajax({
            type: type,
            url: url,
            async: async,
            data: jsonData,
            dataType: 'json',
            success: function (retorno) {
                if(mostrarBloqueo){
                    ocultarBloqueoAjax();
                }

                if (retorno.Success) {
                    /* Todo bien */
                    functionSuccess(retorno.Objeto);
                }
                else {
                    /* Error */
                    var msj = "Se generó un error: " + retorno.MensajeError;

                    if (functionError == null || functionError == 'undefined') {
                        alert(msj);
                    } else {
                        functionError(msj);
                    }
                }
            },
            error: function (objeto, title, mensaje) {
                if(mostrarBloqueo){
                    ocultarBloqueoAjax();
                }

                if (functionError == null || functionError == 'undefined') {
                    alert(title + ": " + mensaje + "\n\nDetalle error:\n  - Status: " + objeto.status + "\n  - Descripción: " + objeto.statusText);
                } else {
                    functionError(title + ": " + mensaje + "\n\nDetalle error:\n  - Status: " + objeto.status + "\n  - Descripción: " + objeto.statusText);
                }
            }
        });
    }

    function mostrarBloqueoAjax(msgAjax, msgProgreso) {
        $.blockUI({
            message: '<h1>' + msgProgreso + '</h1>',
            css: { backgroundColor: 'rgba(0, 0, 0, .5)', color: '#c5c5c5', borderRadius: '9px', border: 'none !important', height: '70px', padding: '40px 0 0 0', fontSize: '9px', left: '33%', width: '35%'}
        });
    }

    function ocultarBloqueoAjax() {
        $.unblockUI();
    }

});