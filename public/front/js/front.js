$(document).ready(function() {
    var $grupoRazonSocial = $('#grupo_razon_social');
    var $grupoNombre = $('#grupo_apellido_nombre');
    var $razonSocial = $('#razon_social');
    var $nombre = $('#nombre');
    var $apellido = $('#apellido');

    $("#infoMensaje1").hide();
    $("#infoMensaje2").hide();

    $(document).on('change', '#tipo_persona', function () {
        if(this.value === '1') {
            $grupoRazonSocial.addClass('d-none');
            $grupoNombre.removeClass('d-none');
            $razonSocial.val('');
        } else if(this.value === '2') {
            $grupoNombre.addClass('d-none');
            $grupoRazonSocial.removeClass('d-none');
            $nombre.val('');
            $apellido.val('');
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

    autoGuionCUIT($("#cuit"));
    autoGuionCUIT($("#cuit_consorcio"));



    $('#formulario').validate({
        lang: 'es',
        rules: {
            'matricula': {
                number: true
            },
            'cuit': {
                cuit: true
            },
            'cuit_consorcio': {
                cuit: true
            },
            'altura': {
                number: true
            }
        },
        submitHandler: function(form) {
            // your ajax would go here
            return false; // blocks regular submit since you have ajax
        }
    });

    $("#btnAlert").click(function() {
        closeAlert();
    });

    $("#crear_pdf").click(function() {
        crearPdf();
    });

    //busquedaPorUrl();

    $("#buscar").click(function() {
        $('form').valid();
        buscar(false);
    });

    $("#limpiar").click(function() {
        limpiarFiltros();
    });

    $("#tableAdministradores").DataTable({
        "paging": false,
        "info": false,
        "destroy": true,
        "searching": false,
        "lengthChange": false,
        "processing": false,
        "data": [],
        "responsive": true,
        "columns": [
            {
                "data": "CUITCONSORCIO",
                "searchable": false,
                "visible": true,
                defaultContent: ""
            },
            {
                "searchable": false,
                "visible": true,
                defaultContent: "",
                width: 150,
                className: "text-center",
                data: null,
                sortable: false,
                render: function(data, type, row, meta) {
                    var res = row.CALLE + ', ' + row.ALTURA;
                    return res;
                }
            },
            {
                "data": "RAZONSOCIAL",
                "searchable": false,
                "visible": true,
                defaultContent: ""
            },
            {
                "data": "CUIT",
                "searchable": false,
                "visible": true,
                defaultContent: ""
            },
            {
                "data": "MATRICULAID",
                "searchable": false,
                "visible": true,
                defaultContent: ""
            },
            {
                "searchable": false,
                "visible": true,
                defaultContent: "",
                width: 150,
                className: "text-center",
                data: null,
                sortable: false,
                render: function(data, type, row, meta) {
                    var res = '<button type="button" id="detalle" class="btn btn-secondary btn-sm" onclick="verMas(' + meta.row + ');"></i>Detalle</button>';
                    return res;
                }
            }
        ],
    });

    $("#tableAdministradores_wrapper").css('display', 'block');

});