function closeAlert() {
    $("#alert").hide();
}

function limpiarFiltros() {
    $("#cuit").val('');
    $("#matricula").val('');
    $("#tipo_persona").val('0');
    $("#razon_social").val('');
    $("#nombre").val('');
    $("#apellido").val('');
    $("#calle").val('');
    $("#altura").val('');
    $("#cuit_consorcio").val('');
    $("#tableAdministradores").dataTable().fnClearTable();
    $("#infoMensaje1").hide();
    $("#infoMensaje2").hide();
}

function buscar(porUrl) {

    if (!validarBusqueda())
        return;

    $("#tableAdministradores").dataTable().fnClearTable();

    var dataBuscar = {
        'cuit': $("#cuit").val(),
        'matricula': $("#matricula").val(),
        'tipoPersona': $("#tipo_persona").val(),
        'razonSocial': $("#razon_social").val(),
        'nombre': $("#nombre").val(),
        'apellido': $("#apellido").val(),
        'calle': $("#calle").val(),
        'altura': $("#altura").val(),
        'cuitConsorcio': $("#cuit_consorcio").val()
    };

    ajax('POST', "/administradores", dataBuscar, 'Espere por favor...', 'Cargando Administradores...', true, function (retorno) {
        //funcion para el success, se accede al objeto como retorno.Objeto
        if (retorno != null && retorno.length > 0) {
            if (retorno.length > 500) {
                cambiarAlert("info", "Por favor aplique mas filtros", "La búsqueda arrojó demasiados resultados.");
                $("#alert").show();
                return;
            }
            if (retorno != null && retorno != undefined) {
                var table = $("#tableAdministradores").DataTable();
                table.clear().draw();
                table.rows.add(retorno).draw();

                if (porUrl)
                    verMas(table.rows[0]);

                mostrarMensajes(retorno[0]);
            }
        } else {
            cambiarAlert("info", "Por favor aplique mas filtros", "No hay coincidencias.");
            $("#alert").show();
            return;
        }

    }, function (mensaje) {
        //funcion para el error, puede ser optativo
        cambiarAlert("error", "Error", "Ocurrio un error.");
        $("#alert").show();
    }, false);

}

function mostrarMensajes(retorno) {
    $("#infoMensaje1").hide();
    $("#infoMensaje2").hide();

    if (retorno.MENSAJE1 != undefined || retorno.MENSAJE1 != '') {
        $("#infoMensaje1").show();
        $("#titulo_alert_info_mensaje1").html("Mensaje Informativo 1");
        $("#cuerpo_alert_info_mensaje1").html(retorno.MENSAJE1);
    }
    if (retorno.MENSAJE2 != undefined || retorno.MENSAJE2 != '') {
        $("#infoMensaje2").show();
        $("#titulo_alert_info_mensaje2").html("Mensaje Informativo 2");
        $("#cuerpo_alert_info_mensaje2").html(retorno.MENSAJE2);
    }
}

function validarBusqueda() {
    if ($("#cuit").val() == '' && $("#matricula").val() == '' && $("#tipo_persona").val() == '0' &&
        $("#razon_social").val() == '' && $("#nombre").val() == '' && $("#apellido").val() == '' && $("#calle").val() == '' &&
        $("#altura").val() == '' && $("#cuit_consorcio").val('')) {
        cambiarAlert("info", "Por favor aplique mas filtros", "La búsqueda arrojó demasiados resultados.");
        $("#alert").show();
        return false;
    }

    if ($("#tipo_persona").val() == '0' && ($("#razon_social").val() != '' || $("#nombre").val() != '' || $("#apellido").val() != '')) {
        cambiarAlert("info", "Por favor aplique mas filtros", "Debe seleccionar un tipo de persona");
        $("#alert").show();
        return false;
    }

    if ($("#tipo_persona").val() != '0' && $("#razon_social").val() == '' && $("#nombre").val() == '' && $("#apellido").val() == '') {
        cambiarAlert("info", "Por favor aplique mas filtros", "Debe ingresar un valor para razón social o apellido y nombre");
        $("#alert").show();
        return false;
    }

    if ($("#tipo_persona").val() == '1' && ($("#nombre").val() == '' || $("#apellido").val() == '')) {
        cambiarAlert("info", "Por favor aplique mas filtros", "Debe ingresar un valor para apellido y nombre");
        $("#alert").show();
        return false;
    }

    if ($("#tipo_persona").val() == '2' && $("#razon_social").val() == '') {
        cambiarAlert("info", "Por favor aplique mas filtros", "Debe ingresar un valor para razón social");
        $("#alert").show();
        return false;
    }

    if (($("#calle").val() != '' && $("#altura").val() == '') || ($("#calle").val() == '' && $("#altura").val() != '')) {
        cambiarAlert("info", "Por favor aplique mas filtros", "Debe ingresar calle y altura");
        $("#alert").show();
        return false;
    }
    return true;
}

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function busquedaPorUrl() {
    var bandera = false;

    var url = (window.location != window.parent.location) ?
        document.referrer :
        document.location.href;

    var matriculaid = getParameterByName('matriculaid', url);
    var cuitcorsocio = getParameterByName('cuitconsorcio', url);

    if (matriculaid != null && matriculaid != '' && matriculaid != undefined) {
        $("#matricula").val(matriculaid);
        bandera = true;
    }

    if (cuitcorsocio != null && cuitcorsocio != '' && cuitcorsocio != undefined) {
        $("#cuit_consorcio").val(cuitcorsocio);
        bandera = true;
    }

    if (bandera == true)
        buscar(true);

    bandera = false;

}

function verMas(administrador) {
    var table = $("#tableAdministradores").DataTable();
    var datos = table.row(administrador).data();
    $("#lbl_razon_social").text(datos.RAZONSOCIAL);

    var domicilioConstituido = datos.CALLEADMINISTRADOR + ' ' +
        datos.ALTURAADMINISTRADOR + ' - ' +
        datos.PISOADMINISTRADOR + ' - ' +
        datos.DEPARTAMENTOADMINISTRADOR + ' - ' +
        datos.CPADMINISTRADOR;

    $("#lbl_domicilio_constituido").text(domicilioConstituido);
    $("#lbl_cuit").text(datos.CUIT);
    $("#lbl_cantidad_consorsios").text(datos.CANTIDADCONSORCIOS);
    $("#lbl_matricula").text(datos.MATRICULAID);
    $("#lbl_oneroso").text(datos.ONEROSO);
    $("#lbl_estado_matricula").text(datos.ESTADOMATRICULADESC);
    $("#lbl_fecha_alta").text(moment(datos.FECHAALTA).format("DD/MM/yyyy"));
    $("#lbl_tiene_sanciones").text(datos.TIENESANCIONES);
    $("#lbl_fecha_suspencion").text(moment(datos.FECHASUSPENSION).format("DD/MM/yyyy") == "Invalid date" ? "" : moment(datos.FECHASUSPENSION).format("DD/MM/yyyy"));

    $('#modalMensaje').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#modalMensaje').modal('show');
}

function cambiarAlert(tipo, titulo, mensaje) {
    if (tipo == "error") {

        $("#titulo_alert").html(titulo);
        $("#cuerpo_alert").html(mensaje);
        $("#icon-alert").removeClass("fa-info-circle");
        $("#icon-alert").addClass("fa-times-circle");

        $("#alert").removeClass("alert-info");
        $("#alert").addClass("alert-danger");

        $("#btnAlert").removeClass("btn-info");
        $("#btnAlert").addClass("btn-danger");

    } else {

        $("#titulo_alert").html(titulo);
        $("#cuerpo_alert").html(mensaje);

        $("#icon-alert").removeClass("fa-times-circle");
        $("#icon-alert").addClass("fa-info-circle");

        $("#alert").removeClass("alert-danger");
        $("#alert").addClass("alert-info");

        $("#btnAlert").removeClass("btn-danger");
        $("#btnAlert").addClass("btn-info");
    }

    window.setTimeout(function () {
        $("#alert").fadeOut(300)
    }, 5000);

}

function crearPdf() {
    html2canvas(document.getElementById("modalMensaje"), {
        onrendered: function (canvas) {

            var imgData = canvas.toDataURL(
                'image/png');
            var doc = new jsPDF('p', 'in', 'letter');
            doc.addImage(imgData, 'PNG',0, 0);
            doc.save('sample-file.pdf');
        }
    });


    // var pdf = new jsPDF('p', 'in', 'letter')

    // 	// source can be HTML-formatted string, or a reference
    // 	// to an actual DOM element from which the text will be scraped.
    // 	,
    // 	source = $('#modalMensaje')[0]

    // 	// we support special element handlers. Register them with jQuery-style 
    // 	// ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // 	// There is no support for any other type of selectors 
    // 	// (class, of compound) at this time.
    // 	,
    // 	specialElementHandlers = {
    // 		// element with id of "bypass" - jQuery style selector
    // 		'#no-incluir-pdf': function(element, renderer) {
    // 			// true = "handled elsewhere, bypass text extraction"
    // 			return true
    // 		}
    // 	}

    // // all coords and widths are in jsPDF instance's declared units
    // // 'inches' in this case
    // pdf.fromHTML(
    // 	source // HTML string or DOM elem ref.
    // 	, 0.5 // x coord
    // 	, 0.5 // y coord
    // 	, {
    // 		'width': 7.5 // max width of content on PDF
    // 			,
    // 		'elementHandlers': specialElementHandlers
    // 	}
    // )

    // var string = pdf.output('datauristring');
    // var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"
    // var x = window.open();
    // x.document.open();
    // x.document.write(iframe);
    // x.document.close();
}