﻿/**
* Funciones globales a la aplicación.
*/
var TiposDomicilios = [{ TipoDomicilioId: 1, Descripcion: 'FISCAL' }, { TipoDomicilioId: 2, Descripcion: 'CONSTITUIDO' }, { TipoDomicilioId: 3, Descripcion: 'NOTIFICACIÓN' }, { TipoDomicilioId: 4, Descripcion: 'DENUNCIADO' }, { TipoDomicilioId: 5, Descripcion: 'REAL' }]
var glbActividad = null;

function chequearDescargaAdjunto(urlDescargarAdjunto, IdAdjunto) {

    $.ajax({
        url: urlContent + '_Attachments/AdjuntoTieneBinario',
    contentType: "application/json; charset=utf-8",
    type: "POST",
    data: JSON.stringify({ "idAdjunto": IdAdjunto}),
    dataType: 'json',
    success: function (data) {
        if (data.result == true) {
            window.location = urlDescargarAdjunto + '?idAdjunto=' + IdAdjunto;
        } else {
            alert("No se encontro un documento asociado al poder del apoderado.");
        }
    },
    error: function (request, status, error) {
        console.log(request.responseText);
    }
    });
}
/**
* Obtiene un QueryString partiendo de la clave.
*/
function getQueryString(key) {
    var queries = window.location.href.split('?')[1];
    if (queries) {
        for (var i = 0; i < queries.split('&').length; i++) {
            if (queries.split('&')[i].split('=')[0] == key) {
                return queries.split('&')[i].split('=')[1];
            }
        }
    }
    return '';
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function ExistValueInTable(tbl, campo, valor) {
    var data = tbl.DataTable().data().toArray();

    for (var d = 0; d < data.length; d++) {
        if (data[d][campo] == valor) {
            return true;
        }
    }
    return false;
}

function completarTxtCUIT(imp) {
    var txt = imp.val().replace(/[^0-9]/g, '');
    var res = '';
    for (i in txt) {
        if (i == 2 || i == 10) {
            res += '-';
        }
        else if (i > 10) {
            break;
        }
        res += txt[i];
    }

    imp.val(res);
}

function autoGuionCUIT(txtBox) {
    txtBox.change(function () {
        completarTxtCUIT($(this));
    });

    txtBox.keyup(function () {
        completarTxtCUIT($(this));
    });
}

function completarTxtHora(imp) {
    var txt = imp.val().replace(/[^0-9]/g, '');
    var res = '';
    for (i in txt) {
        if (i == 2) {
            res += ':';
        }
        else if (i > 3) {
            break;
        }
        res += txt[i];
    }

    imp.val(res);
}

function autoHora(txtBox) {
    txtBox.change(function () {
        completarTxtHora($(this));
    });

    txtBox.keyup(function () {
        completarTxtHora($(this));
    });
}

function autoCompleteCalle(txt, url, provincia) {
    $('#' + txt).typeahead({
        source: function (request, process) {
            if ($('#' + provincia + ' option:selected').text() == 'CABA') {
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    type: "POST",
                    data: JSON.stringify({ "calle": "" + this.value }),
                    dataType: 'json',
                    success: function (data) {
                        if (data != "") {
                            var resultado = JSON.parse(data);
                            var domicilio = [];
                            var map = {};

                            $.each(resultado.direccionesNormalizadas, function (i, dom) {
                                map.id = i;
                                map.name = dom.nombre_calle; //cambiar por nombre calle
                                domicilio.push(dom.nombre_calle);
                            });
                            return process(domicilio);
                        }
                        return process("");

                    },
                    error: function (request, status, error) {
                        console.log(request.responseText);
                    }
                });
            }
        },
        autoSelect: true,
        minLength: 4,
        displayKey: 'nombre_calle' //cambiar esto tambien
    });
}

function autoCompleteDomicilioNormalizado(txtCalle, txtNroCalle, txtDomicilioNorm, url, provincia) {
    if ($('#' + provincia + ' option:selected').text() == 'CABA') {
        var calle = $.trim($('#' + txtCalle).val());
        var nro = $('#' + txtNroCalle).val();
        var direccion = calle + " " + nro;
        var encontro = false;

        $('#' + txtDomicilioNorm).val('');

        $.ajax({
            url: url,
            contentType: "application/json; charset=utf-8",
            type: "POST",
            data: JSON.stringify({ "calle": "" + direccion }),
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data != "") {
                    var resultado = JSON.parse(data);
                    if (resultado.direccionesNormalizadas.length != 0 && calle != '' && nro != '') {
                        $.each(resultado.direccionesNormalizadas, function (i, dom) {

                            if (dom.nombre_calle == calle) {
                                $('#' + txtDomicilioNorm).val(dom.direccion);
                                encontro = true;
                                return;
                            } 
                        });

                        if(!encontro)
                            $('#' + txtDomicilioNorm).val("Calle inexistente: " + direccion + ", " + $('#' + provincia + ' option:selected').text());
                    }
                    else {
                        $('#' + txtDomicilioNorm).val(resultado.errorMessage);
                    }
                }
                else {
                    $('#' + txtDomicilioNorm).val("Error de servicio");
                }
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
        });
    }
}

function autoCompleteDatosutiles(txtCalle, txtNroCalle, txtComuna, txtBarrio, txtCP, url, provincia) {
    if ($('#' + provincia + ' option:selected').text() == 'CABA') {
        var calle = $('#' + txtCalle).val();
        var altura = $('#' + txtNroCalle).val();
        var comuna = $('#' + txtComuna);
        var barrio = $('#' + txtBarrio);
        $.ajax({
            url: url,
            contentType: "application/json; charset=utf-8",
            type: "POST",
            data: JSON.stringify({ "calle": "" + calle, "altura": altura }),
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data != "") {
                    var resultado = JSON.parse(data);
                    if (txtCP != "") $('#' + txtCP).val(resultado.codigo_postal);
                    comuna.val(resultado.comuna);
                    barrio.val(resultado.barrio);
                }
                else {
                    if (txtCP != "") $('#' + txtCP).val("Error de servicio");
                    comuna.val("Error de servicio");
                    barrio.val("Error de servicio");
                }

            },

            error: function (request, status, error) {
                console.log(request.responseText);
            }
        });
    }
}



function autoCompleteDatosutiles(txtCalle, txtNroCalle, txtComuna, txtBarrio, txtCP, url, provincia, hdnComisaria) {
    if ($('#' + provincia + ' option:selected').text() == 'CABA') {
        var calle = $('#' + txtCalle).val();
        var altura = $('#' + txtNroCalle).val();
        var comuna = $('#' + txtComuna);
        var barrio = $('#' + txtBarrio);
        var comisaria = $('#' + hdnComisaria);

        if (calle == "" && altura == "")
            return;

        $.ajax({
            url: url,
            contentType: "application/json; charset=utf-8",
            type: "POST",
            data: JSON.stringify({ "calle": "" + calle, "altura": altura }),
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data != "") {
                    var resultado = JSON.parse(data);
                    if (txtCP != "") $('#' + txtCP).val(resultado.codigo_postal);
                    comuna.val(resultado.comuna);
                    barrio.val(resultado.barrio);
                    comisaria.val(resultado.comisaria_vecinal)
                }
                else {
                    if (txtCP != "") $('#' + txtCP).val("Error de servicio");
                    comuna.val("Error de servicio");
                    barrio.val("Error de servicio");
                }

            },

            error: function (request, status, error) {
                console.log(request.responseText);
            }
        });
    }
}


function GetLocalidades(idProvincia) {
    var res = Array();
    if (typeof idProvincia !== 'undefined' && idProvincia !== null) {
        var params = {};
        params.idProvincia = idProvincia;
        // cabecera
        $.ajax({
            async: false,
            type: "POST",
            url: urlContent + '_DomiciliosNormalizados/GetLocalidades',
            data: JSON.stringify(params),
            contentType: "application/json; charset=utf-8",
            dataType: "JSON",
            success: function (resp) {
                res = resp;
            },
            error: function (jqXHR, textStatus, errorThrown) { alert(textStatus); }
        });
    }
    return res;
}

function actualizarListaLocalidad(selProvincia, selLocalidad, selComuna, selBarrio) {
    $("div[data-group-key='" + selLocalidad + "']").each(function () {
        $(this).hide();
    });
    $("div[data-group-key='" + selComuna + "']").each(function () {
        $(this).hide();
    });
    $("div[data-group-key='" + selBarrio + "']").each(function () {
        $(this).hide();
    });

    if ($(selProvincia).find(":selected").text() == 'CABA') {
        $("div[data-group-key='" + selComuna + "']").each(function () {
            $(this).show();
        });
        $("div[data-group-key='" + selBarrio + "']").each(function () {
            $(this).show();
        });
    }
    else {
        var localidades = GetLocalidades($(selProvincia).val());

        $("#" + selLocalidad).html('');

        if (localidades != undefined && localidades != null) {
            for (var l = 0; l < localidades.length; l++) {
                $("#" + selLocalidad).append('<option value="' + localidades[l].LocalidadId + '">' + localidades[l].Descripcion + '</option>');
            }
        }

        $("div[data-group-key='" + selLocalidad + "']").each(function () {
            $(this).show();
        });
    }
}

function eventosDomicilio(idDOM) {
    var urlNormalizarDom = urlContent + '_DomiciliosNormalizados/normalizarDomicilio';
    var urlDatosU = urlContent + '_DomiciliosNormalizados/datosUtiles';

    $('#selProvincia' + idDOM).chosen({ no_results_text: "Sin resultados: " });
    $('#errorNormalizar' + idDOM).hide();

    autoCompleteCalle("txtCalle" + idDOM, urlNormalizarDom, "selProvincia" + idDOM);

    $("#txtAltura" + idDOM).change(function (e) {
        autoCompleteDomicilioNormalizado("txtCalle" + idDOM, this.name, "txtDomicilioNormalizado" + idDOM, urlNormalizarDom, "selProvincia" + idDOM);
        var txtDomicilioNormalizado = $("#txtDomicilioNormalizado" + idDOM).val();
        var arregloDeSubCadenas = txtDomicilioNormalizado.split(" ", 2);

        if (!(arregloDeSubCadenas[0] == "Calle" && arregloDeSubCadenas[1] == "inexistente:")) {
            autoCompleteDatosutiles("txtCalle" + idDOM, "txtAltura" + idDOM, "txtComuna" + idDOM, "txtBarrio" + idDOM, "txtCP" + idDOM, urlDatosU, "selProvincia" + idDOM);
        }
    });

    $("#selProvincia" + idDOM).change(function () {
        $('#selProvincia' + idDOM).trigger('chosen:updated');
        actualizarListaLocalidad(this, "selLocalidad" + idDOM, "txtComuna" + idDOM, "txtBarrio" + idDOM);

        var disableCP = ($("#selProvincia" + idDOM + " option:selected").text() == "CABA");
        $("#txtCP" + idDOM).prop("disabled", disableCP);
        $('#txtDomicilioNormalizado' + idDOM).val('');
    });

    $("#selProvincia" + idDOM).change();
}

function limpiarDomicilioNormalizado(txtCP, txtComuna, txtBarrio, txtDomicilioNorm) {
    if (txtCP != "") $('#' + txtCP).val("");
    $('#' + txtComuna).val("");
    $('#' + txtBarrio).val("");
    $('#' + txtDomicilioNorm).val('');
}

function cerrarAbrirModal(idMdlCerrar, idMdlAbrir) {
    if (idMdlCerrar) {
        $('#' + idMdlCerrar).modal('hide');
    }

    if (idMdlAbrir) {
        $('#' + idMdlAbrir).modal('show');

        $('#' + idMdlAbrir + ' table').each(function () {
            $(this).DataTable().draw();
        });
    }
    // limpio los mensajes de validaciones
    $("form").validate().resetForm();
}

function deshabilitarEnter() {
    $("form").keypress(function (e) {
        if (e.which == 13) {
            return false;
        }
    });
}

function validacionesDocumento(selTipo, txtNro, selPais, requerido) {
    requerido = (requerido != undefined) ? requerido : true;
    selTipo.change(function () {
        var val = parseInt($(this).val());

        if (val == 3) {
            txtNro.rules('remove', 'regex');
        }
        else {
            txtNro.rules('add', { regex: "^\\d+$" });
        }

        //valido solo el formato
        txtNro.rules('remove', 'required');
        txtNro.valid();
        if (requerido) txtNro.rules('add', { required: true });

        if (selPais != undefined) {
            if (val == 3 || val == 4) {
                selPais.prop('disabled', false);
            }
            else {
                selPais.prop('disabled', true);
                selPais.val(1);
            }
        }
    });
    selTipo.change();
}

function MapearDomicilio(oPersona, nomAtt, idDOM, tipoDomicilioId) {
    var val =
        ($('#txtCalle' + idDOM).val() != '' && $('#txtCalle' + idDOM).val() != undefined) &&
        ($('#txtAltura' + idDOM).val() != '' && $('#txtAltura' + idDOM).val() != undefined) &&
        ($('#txtCP' + idDOM).val() != '' && $('#txtCP' + idDOM).val() != undefined);

    if (val) {
        if (!oPersona[nomAtt])
            oPersona[nomAtt] = {};

        oPersona[nomAtt].ProvinciaId = $('#selProvincia' + idDOM).val();
        oPersona[nomAtt].Provincia = {};
        oPersona[nomAtt].Provincia.ProvinciaId = $('#selProvincia' + idDOM).val();
        oPersona[nomAtt].Provincia.Descripcion = $('#selProvincia' + idDOM + ' :selected').text();

        oPersona[nomAtt].LocalidadId = $('#selLocalidad' + idDOM).val();
        oPersona[nomAtt].Localidad = {};
        oPersona[nomAtt].Localidad.LocalidadId = $('#selLocalidad' + idDOM).val();
        oPersona[nomAtt].Localidad.Descripcion = $('#selLocalidad' + idDOM + ' :selected').text();

        oPersona[nomAtt].Comuna = $('#txtComuna' + idDOM).val();
        oPersona[nomAtt].Barrio = $('#txtBarrio' + idDOM).val();
        oPersona[nomAtt].Calle = $('#txtCalle' + idDOM).val();
        oPersona[nomAtt].Altura = $('#txtAltura' + idDOM).val();
        oPersona[nomAtt].Piso = $('#txtPiso' + idDOM).val();
        oPersona[nomAtt].Departamento = $('#txtDepto' + idDOM).val();
        oPersona[nomAtt].CP = $('#txtCP' + idDOM).val();

        var lstTipos = $.grep(TiposDomicilios, function (t) { return t.TipoDomicilioId == tipoDomicilioId; });
        if (lstTipos.length > 0) {
            oPersona[nomAtt].TipoDomicilio = lstTipos[0];
        }
        else {
            oPersona[nomAtt].TipoDomicilio = null;
        }
    }
    else {
        oPersona[nomAtt] = null;
    }
}

function LlenarDomicilio(oPersona, nomAtt, idDOM) {
    if (oPersona[nomAtt]) {
        //respetar orden para la integración USIG
        $('#txtCP' + idDOM).val(oPersona[nomAtt].CP);
        $('#txtComuna' + idDOM).val(oPersona[nomAtt].Comuna);
        $('#txtBarrio' + idDOM).val(oPersona[nomAtt].Barrio);
        $('#txtPiso' + idDOM).val(oPersona[nomAtt].Piso);
        $('#txtDepto' + idDOM).val(oPersona[nomAtt].Departamento);
        $('#selProvincia' + idDOM).val(
            (oPersona[nomAtt].ProvinciaId) ? oPersona[nomAtt].ProvinciaId : ProvinciaIdDefault
        ).change();
        $('#selProvincia' + idDOM).trigger("chosen:updated");

        if (oPersona[nomAtt].LocalidadId)
            $('#selLocalidad' + idDOM).val(oPersona[nomAtt].LocalidadId).change();

        $('#selLocalidad' + idDOM).trigger("chosen:updated");
        $('#txtCalle' + idDOM).val(oPersona[nomAtt].Calle);
        $('#txtAltura' + idDOM).val(oPersona[nomAtt].Altura).change();
    }
    else {
        LimpiarDomicilio(idDOM)
    }
}

function LimpiarDomicilio(idDOM) {
    $('#txtCP' + idDOM).val('');
    $('#selProvincia' + idDOM).val(1).change();
    $('#selLocalidad' + idDOM).val($('#selLocalidad' + idDOM + ' option:first').val()).change();
    $('#txtComuna' + idDOM).val('');
    $('#txtCalle' + idDOM).val('');
    $('#txtAltura' + idDOM).val('');
    $('#txtPiso' + idDOM).val('');
    $('#txtDepto' + idDOM).val('');
    $('#txtBarrio' + idDOM).val('');
    $('#txtDomicilioNormalizado' + idDOM).val('');
}

function BuscarPersona(personaId, tipoPersona, tipoDocumento, paisEmision, nroDocumento, sexo) {
    var res = null;
    var params = {};
    params.personaId = (personaId) ? personaId : 0;
    params.tipoPersona = (tipoPersona) ? tipoPersona : 0;
    params.tipoDocumento = (tipoDocumento) ? tipoDocumento : 0;
    params.paisEmision = (paisEmision) ? paisEmision : 0;
    params.nroDocumento = (nroDocumento) ? nroDocumento : '';
    params.sexo = (sexo) ? sexo : '';

    // cabecera
    $.ajax({
        async: false,
        type: "POST",
        url: urlContent + '_Personas/BuscarPersona',
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        success: function (resp) {
            if (resp.PersonaId) {
                res = resp;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) { alert(textStatus); }
    });

    return res;
}

function BuscarPersonaIntegrada(tipoPersona, tipoDocumento, nroDocumento, sexo, esDenunciante, functionSuccess) {

    mostrarBloqueoAjax("Buscar Persona", "Buscando...");

    var res = null;
    var params = {};
    params.tipoPersona = (tipoPersona) ? tipoPersona : 0;
    params.tipoDocumento = (tipoDocumento) ? tipoDocumento : 0;
    params.nroDocumento = (nroDocumento) ? nroDocumento : '';
    params.sexo = (sexo) ? sexo : '';
    params.esDenunciante = (esDenunciante) ? esDenunciante : false;

    //esto es para que aparezca el bloqueo en chrome
    setTimeout(function () {
        // cabecera
        $.ajax({
            async: false,
            type: "POST",
            url: urlContent + '_Personas/BuscarPersonaIntegrada',
            data: JSON.stringify(params),
            contentType: "application/json; charset=utf-8",
            dataType: "JSON",
            success: function (resp) {
                if (resp.PersonaId || resp.NroDocumento != null) {
                    res = resp;
                }

                if (functionSuccess != null && functionSuccess != undefined)
                    functionSuccess(res);
            },
            error: function (jqXHR, textStatus, errorThrown) { alert(textStatus); }
        });
    }, 200);

    ocultarBloqueoAjax();

    return res;
}

function BuscarMatricula(tipoPersona, matricula, functionSuccess) {

    mostrarBloqueoAjax("Buscar Persona", "Buscando...");

    var res = null;
    var params = {};
    params.tipoPersonaId = (tipoPersona) ? tipoPersona : 0;
    params.matricula = matricula;
    // cabecera
    $.ajax({
        async: true,
        type: "POST",
        url: urlContent + '_Personas/BuscarMatricula',
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        success: function (resp) {
            if (resp.PersonaId || resp.NroDocumento != null) {
                res = resp;
            }

            ocultarBloqueoAjax();

            if (functionSuccess != null && functionSuccess != undefined)
                functionSuccess(res);
        },
        error: function (jqXHR, textStatus, errorThrown) { ocultarBloqueoAjax(); alert(textStatus); }
    });

}

function MapearApoderado(nuevoApododerado, idDOM) {
    if (nuevoApododerado) {
        var res = nuevoApododerado;
    }
    else {
        var res = {};
    }

    res.TipoDocumentoId = $('#selTipoDocumentoApoderado' + idDOM).val();
    res.TipoDocumento = {};
    res.TipoDocumento.TipoDocumentoId = $('#selTipoDocumentoApoderado' + idDOM).val();
    res.TipoDocumento.Descripcion = $('#selTipoDocumentoApoderado' + idDOM + ' :selected').text();
    res.NroDocumento = $('#txtNroDocumentoApoderado' + idDOM).val();
    res.Nombre = $('#txtNombreApoderado' + idDOM).val();
    res.Apellido = $('#txtApellidoApoderado' + idDOM).val();
    res.PoderAdjuntoId = null;
    res.AdjuntoId = null;
    res.PoderPresentado = $('#hdnPoderPresentado' + idDOM).val() == 'true';
    res.IdPaisEmision = $('#selPaisdeEmisionApoderado' + idDOM).val();
    res.Sexo = $('#selSexoApoderado' + idDOM).val();

    res.Autorizado = $('#chkAutorizadoApoderado' + idDOM).prop('checked');
    res.Telefono = $('#txtTelefonoApoderado' + idDOM).val();
    res.Tomo = $('#txtTomoApoderado' + idDOM).val();
    res.Folio = $('#txtFolioApoderado' + idDOM).val();

    res.PersonaId = $('#hdnPersonaIdApoderado' + idDOM).val();
    return res;
}

function NewApoderado(idDOM) {
    $('#hdnPoderPresentado' + idDOM).val(false);

    $('#selTipoDocumentoApoderado' + idDOM).val(1).change();
    $('#txtNroDocumentoApoderado' + idDOM).val('');
    $('#selSexoApoderado' + idDOM).val('M');

    $('#txtNombreApoderado' + idDOM).val('').prop('readonly', true);
    $('#txtApellidoApoderado' + idDOM).val('').prop('readonly', true);

    cerrarAbrirModal('mdlBuscarApoderado' + idDOM, 'mdlApoderado' + idDOM);
}

function BuscarApoderado(nuevoApododerado, idDOM) {
    var val = $('#txtNroDocumentoApoderado' + idDOM).valid();

    if (val) {
        var nroDocumento = $('#txtNroDocumentoApoderado' + idDOM).val();
        var tipoDocumento = $('#selTipoDocumentoApoderado' + idDOM).val();
        var paisEmision = $('#selPaisdeEmisionApoderado' + idDOM).val();
        var sexo = $('#selSexoApoderado' + idDOM).val();
        var tipoPersona = 1;

        $('#txtNombreApoderado' + idDOM).val('').prop('readonly', true);
        $('#txtApellidoApoderado' + idDOM).val('').prop('readonly', true);

        nuevoApododerado = BuscarPersonaIntegrada(tipoPersona, tipoDocumento, nroDocumento, sexo, false, function (objPersona) {
           
            if (objPersona == null) {
                alert('- No se encontró datos para la Persona que está buscando.\n');
                $('#hdnPersonaIdApoderado' + idDOM).val(0);
                $('#txtNombreApoderado' + idDOM).prop('readonly', false);
                $('#txtApellidoApoderado' + idDOM).prop('readonly', false);
            }
            else {
                $('#hdnPersonaIdApoderado' + idDOM).val(objPersona.PersonaId);
                $('#txtNombreApoderado' + idDOM).val(objPersona.Nombre);
                $('#txtApellidoApoderado' + idDOM).val(objPersona.Apellido);
            }
        });
    }

    return nuevoApododerado;
}

function validateInputs(input) {
    var valid = true;
    if (input.length > 0) {
        if (input.indexOf('<') != -1 || input.indexOf('&') != -1 || input.indexOf('#') != -1) {
            if (input.indexOf('< ') != -1)
                valid = true
            else
                valid = false;
        }

    }
    return valid;
}

function mostrarBloqueoAjax(msgAjax, msgProgreso) {
    $.blockUI({
        message: '<h1>' + msgProgreso + '</h1>',
        css: { backgroundColor: 'rgba(0, 0, 0, .5)', color: '#c5c5c5', borderRadius: '9px', border: 'none !important', height: '90px', padding: '0px 0 0 0', fontSize: '9px', left: '33%', width: '35%' },
        baseZ: 2000,
        //forceIframe: true,
        // fadeIn time in millis; set to 0 to disable fadeIn on block 
        fadeIn:  0, 
 
        // fadeOut time in millis; set to 0 to disable fadeOut on unblock 
        fadeOut: 0,
    });
}

function ocultarBloqueoAjax() {
    $.unblockUI();
}

function workingDays(dateFrom, dateTo) {
    var from = moment(dateFrom, 'DD/MM/YYY'),
      to = moment(dateTo, 'DD/MM/YYY'),
      days = 0;

    while (!from.isAfter(to)) {
        // Si no es sabado ni domingo
        if (from.isoWeekday() !== 6 && from.isoWeekday() !== 7) {
            days++;
        }
        from.add(1, 'days');
    }
    return days;
}
function validarCUIT(value) {
    if (value != '') {
        // tiene la forma 00-00000000-0 ? 
        var listtieneFormato = value.match(/^\d{2}\-\d{8}\-\d{1}$/);
	var tieneFormato = false;
        if(listtieneFormato != null){
		tieneFormato = true;
}
        // validar dígito verificador
        var digitoValido = false;

        var sCUIT = value.replace(/[^0-9]/g, '');
        var aMult = '5432765432';
        var aMult = aMult.split('');

        if (sCUIT && sCUIT.length == 11) {
            aCUIT = sCUIT.split('');
            var iResult = 0;
            for (i = 0; i <= 9; i++) {
                iResult += aCUIT[i] * aMult[i];
            }
            iResult = (iResult % 11);
            iResult = 11 - iResult;

            if (iResult == 11) iResult = 0;
            if (iResult == 10) iResult = 9;

            if (iResult == aCUIT[10]) {
                digitoValido = true;
            }
        }

        return value == '' || (tieneFormato && digitoValido);
    }
    else {
        return true;
    }
}
function ValidarExisteEnTabla(tbl, campo, valor, rowIdx, msg) {
    var table = tbl.DataTable();
    rowIdx = (rowIdx == '') ? -1 : rowIdx;
    msg = (msg == '') ? 'un registro' : msg;

    for (var d = 0; d < table.data().toArray().length; d++) {
        var row = table.row(d).data();

        if (rowIdx != d && row[campo] == valor) {
            alert('- Ya existe ' + msg + ' "' + valor + '"');
            return false;
        }
    }
    return true;
}

function DeleteRowConfirm(tbl, btn, nombre) {
    nombre = (nombre == undefined) ? 'este registro' : nombre;
    if (confirm("¿Esta seguro de eliminar " + nombre + "?")) {
        tbl.DataTable().row($(btn).parents('tr')).remove().draw(false);
    }
}

function ObtenerActividadPectra() { 
    if (glbActividad == null) {

        var params = {};
        params.TrxId = $("#TrxId").val();

        $.ajax({
            async: false,
            type: "POST",
            url: urlContent + '_AccionJuridico/GetActividad',
            data: JSON.stringify(params),
            contentType: "application/json; charset=utf-8",
            dataType: "JSON",
            success: function (resp) {
                if (resp) {
                    glbActividad = resp;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { alert(textStatus); }
        });
    }

    return glbActividad;
}

function mostrarMensajeTelefono(txtNro, mensajeTelefono) {

    var msg = "";

    if (txtNro.attr('telefono-valido') != undefined) {
        if (txtNro.attr('telefono-valido') == 'false')
            msg = " - El Código + número de " + mensajeTelefono + " debe estar conformado por 10 caracteres. \n";
    }
    return msg;

}