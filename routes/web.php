<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //echo phpinfo();
    return redirect('/administradores');
});

Route::group(['namespace' => 'Front'], function () {
    Route::get('/administradores', [
        'uses' => 'AdministratorController@search',
        'as'   => 'front.administrators.index'
    ]);

    Route::post('/administradores', [
        'uses' => 'AdministratorController@searching',
        'as'   => 'front.administrators.search'
    ]);
});
