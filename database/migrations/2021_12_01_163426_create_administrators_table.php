<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdministratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrators', function (Blueprint $table) {
            $table->integer('MATRICULAID')->nullable();
            $table->string('CUIT', 30)->nullable();
            $table->string('ONEROSO', 10)->nullable();
            $table->integer('ESTADOMATRICULAID')->nullable();
            $table->string('ESTADOMATRICULADESC', 50)->nullable();
            $table->integer('CANTIDADCONSORCIOS')->nullable();
            $table->string('RAZONSOCIAL', 100)->nullable();
            $table->dateTime('FECHAALTA')->nullable();
            $table->dateTime('FECHABAJA')->nullable();
            $table->dateTime('FECHASUSPENSION')->nullable();
            $table->dateTime('FECHAVENCIMIENTODDJJ')->nullable();
            $table->dateTime('FECHAACTUALIZACION')->nullable();
            $table->string('TIENESANCIONES', 10)->nullable();
            $table->integer('TIPOPERSONAID')->nullable();
            $table->string('TIPOPERSONADESC', 30)->nullable();
            $table->integer('TIPODOCUMENTOID')->nullable();
            $table->string('TIPODOCUMENTODESC', 30)->nullable();
            $table->string('NRODOCUMENTO', 50)->nullable();
            $table->string('SEXO', 30)->nullable();
            $table->string('CALLE', 50)->nullable();
            $table->string('ALTURA', 20)->nullable();
            $table->string('PISO', 10)->nullable();
            $table->string('DEPARTAMENTO', 10)->nullable();
            $table->string('CP', 30)->nullable();
            $table->string('COMUNAUSIG', 50)->nullable();
            $table->string('BARRIOUSIG', 50)->nullable();
            $table->integer('PROVINCIAID')->nullable();
            $table->string('TELEFONO', 30)->nullable();
            $table->string('CELULAR', 30)->nullable();
            $table->string('CORREOELECTRONICO', 100)->nullable();
            $table->string('CUITCONSORCIO', 30)->nullable();
            $table->integer('CONSORCIOID')->nullable();
            $table->string('CALLEADMINISTRADOR', 50)->nullable();
            $table->string('ALTURAADMINISTRADOR', 20)->nullable();
            $table->string('PISOADMINISTRADOR', 10)->nullable();
            $table->string('DEPARTAMENTOADMINISTRADOR', 10)->nullable();
            $table->string('CPADMINISTRADOR', 30)->nullable();
            $table->string('COMUNAUSIGADMINISTRADOR', 50)->nullable();
            $table->string('BARRIOUSIGADMINISTRADOR', 50)->nullable();
            $table->integer('PROVINCIAIDADMINISTRADOR')->nullable();
            $table->integer('MENSAJEID1')->nullable();
            $table->longText('MENSAJE1')->nullable();
            $table->integer('MENSAJEID2')->nullable();
            $table->longText('MENSAJE2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrators');
    }
}
