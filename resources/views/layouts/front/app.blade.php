<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Buscador de administradores</title>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,700;1,400&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700&display=swap" rel="stylesheet">

    <!-- Stylesheets -->
    {!! Html::style('front/css/obelisco.css') !!}
    {!! Html::style('front/css/style.css') !!}

    <!-- Scripts -->
    {!! Html::script('front/js/jquery.min.js') !!}
    {!! Html::script('front/js/bootstrap.min.js') !!}
    {!! Html::script('front/js/jquery.caret.js') !!}
    {!! Html::script('front/js/jquery-ui.min.js') !!}
    {!! Html::script('front/js/ajax.js') !!}
    {!! Html::script('front/js/jquery.blockUI.js') !!}
    {!! Html::script('front/js/jquery.dataTables.min.js') !!}
    {!! Html::script('front/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('front/js/moment.js') !!}
    {!! Html::script('front/js/jquery.validate.min.js') !!}
    {!! Html::script('front/js/messages_es.js') !!}
    {!! Html::script('front/js/jquery.validate.forms20.js') !!}
    {!! Html::script('front/js/funciones.js') !!}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    {!! Html::script('front/js/jsPDF.js') !!}
    {!! Html::script('front/js/html2canvas.js') !!}
    {!! Html::script('front/js/consultaadministradores.js') !!}
    {!! Html::script('front/js/front.js') !!}
</head>
<body>
    @yield('content')
</body>
</html>
