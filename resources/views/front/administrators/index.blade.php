@extends('layouts.front.app')

@section('content')
    <div class="container">
        <div class="col-sm-12">
            @include('front.administrators.partials.form-filter')
            <div class="row">
                <table id="tableAdministradores" class="table table-bordered table-striped table-responsive-md" cellspacing="0" style="width:100%;">
                    <thead>
                    <tr>
                        <th>Cuit Consorcio</th>
                        <th>Domicilio</th>
                        <th>Nombre Apellido / Razón Social</th>
                        <th>Cuit</th>
                        <th>Matrícula</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal" id="modalMensaje">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Datos Administrador</h4>
                            <button type="button" class="close" id="no-incluir-pdf" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-10">
                                    <strong>Apellido, Nombre / Razón Social</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <label id="lbl_razon_social"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <strong>Domicilio Constituido</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <label id="lbl_domicilio_constituido"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <strong>Matricula</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <label id="lbl_matricula"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <strong>Oneroso</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <label id="lbl_oneroso"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <strong>Estado Matricula</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <label id="lbl_estado_matricula"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <label id="lbl_suspendida"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <strong>Fecha Alta</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <label id="lbl_fecha_alta"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <strong>Tiene Sanciones</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <label id="lbl_sanciones"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <strong>Fecha Suspención</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <label id="lbl_fecha_suspencion"></label>
                                </div>
                            </div>
                            <br>
                            <div class="row" style="display: block;">
                                <div class="col-lg-10">
                                    <div id="infoMensaje1" class="alert-wrapper">
                                        <div class="alert alert-info alert-dismissible fade show infoMensaje">
                                            <p><i id="icon-alert" class="fa fa-info-circle"></i>
                                                <strong id="titulo_alert_info_mensaje1">Mensaje Informativo</strong><br>
                                                <span id="cuerpo_alert_info_mensaje1">Cuerpo</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="display: block;">
                                <div id="infoMensaje2" class="alert-wrapper">
                                    <div class="alert alert-info alert-dismissible fade show infoMensaje">
                                        <p><i id="icon-alert" class="fa fa-info-circle"></i>
                                            <strong id="titulo_alert_info_mensaje2">Mensaje Informativo</strong><br>
                                            <span id="cuerpo_alert_info_mensaje2">Cuerpo</span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" id="no-incluir-pdf">
                            <button id="crear_pdf" type="button" class="btn btn-secondary btn-sm">Crear PDF</button>
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection