<form id="formulario" action="/administradores" method="get">
    <div class="accordion-wrapper">
        <div class="accordion" id="accordionExample">
            <div class="card">
                <button class="card-header card-link" data-toggle="collapse" data-target="#panel_filtros_administrador" aria-expanded="true">
                    Filtros Administrador
                </button>
                <div class="card-body panel-collapse in collapse show" id="panel_filtros_administrador">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for=" text-area">Tipo Persona</label>
                            <select class="form-control" id="tipo_persona">
                                <option value="0">Seleccione</option>
                                <option value="1">Humana</option>
                                <option value="2">Juridica</option>
                            </select>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for=" text-area">Matrícula</label>
                            <input type="text" name="matricula" id="matricula" class="form-control" value="" placeholder="Nro Matrícula">
                            <small class="form-text text-muted">Solo valores numericos.</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for=" text-area">Cuit</label>
                            <input type="text" name="cuit" id="cuit" class="form-control" value="" placeholder="Cuit">
                            <small class="form-text text-muted">Completar sin puntos ni guiones.</small>
                        </div>
                    </div>
                    <div class="row d-none" id="grupo_razon_social">
                        <div class="col-sm-6 form-group">
                            <label for=" text-area">Razón Social </label>
                            <input type="text" name="razon_social" id="razon_social" class="form-control" value="" placeholder="Razón Social">
                        </div>
                    </div>
                    <div class="row d-none" id="grupo_apellido_nombre">
                        <div class="col-sm-6 form-group">
                            <label for=" text-area">Nombre</label>
                            <input type="text" name="nombre" id="nombre" class="form-control" value="" placeholder="Nombre">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for=" text-area">Apellido</label>
                            <input type="text" name="apellido" id="apellido" class="form-control" value="" placeholder="Apellido">
                        </div>

                    </div>
                </div>
            </div>
            <div class="card">
                <button class="card-header card-link" data-toggle="collapse" data-target="#panel_filtro_consorcio" aria-expanded="true">
                    Filtros Consorcios
                </button>
                <div class="card-body panel-collapse in collapse show" id="panel_filtro_consorcio">

                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for=" text-area">Cuit</label>
                            <input type="text" name="cuit_consorcio" id="cuit_consorcio" class="form-control" value="" placeholder="Cuit">
                            <small class="form-text text-muted">Completar sin puntos ni guiones.</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for=" text-area">Calle</label>
                            <input type="text" name="calle" id="calle" class="form-control" value="" placeholder="Calle">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="text-area">Altura</label>
                            <input type="text" name="altura" id="altura" class="form-control" value="" placeholder="Altura">
                            <small class="form-text text-muted">Solo valores numericos.</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert-wrapper">
        <div id="alert" class="alert alert-info alert-dismissible fade show" role="alert">
            <p><i id="icon-alert" class="fa fa-info-circle"></i>
                <strong id="titulo_alert">Titulo</strong><br>
                <span id="cuerpo_alert">Cuerpo</span>
            </p>
            <button type="button" id="btnAlert" class="btn btn-sm btn-info close-btn">Cerrar</button>
        </div>
    </div>
    <div class="form-group align-right">
        <button type="button" id="limpiar" class="btn btn-secondary btn-sm"></i>Limpiar</button>
        <button type="button" id="buscar" class="btn btn-primary btn-sm">Filtrar</button>
    </div>
</form>
