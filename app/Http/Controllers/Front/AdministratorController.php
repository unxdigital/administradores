<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Administrator;
use Illuminate\Support\Facades\Log;

class AdministratorController extends Controller
{
    /**
     * Display a listing of the resource
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function search()
    {
        return view('front.administrators.index');
    }

    public function searching(Request $request)
    {
        if ($request->ajax()) {
            Log::info(sprintf('Request for search: %s', json_encode($request->all())));
            $query = Administrator::cuit($request->input('cuit'))
                ->matricula($request->input('matricula'))
                ->tipoPersona($request->input('tipoPersona'))
                ->razonSocial($request->input('razonSocial'))
                ->nombre($request->input('nombre'))
                ->apellido($request->input('apellido'))
                ->calle($request->input('calle'))
                ->altura($request->input('altura'))
                ->cuitConsorcio($request->input('cuitConsorcio'));

            Log::info(sprintf('Query: %s', $query->toSql()));

            $result = $query->get();

            if (!$result) {
                $return['Success'] = true;
                $return['MensajeError'] = 'No se encontraron resultados';
                $return['Objeto'] = '';
            } else {
                $return['Success'] = true;
                $return['MensajeError'] = '';
                $return['Objeto'] = $result;
            }
        } else {
            $return['Success'] = false;
            $return['MensajeError'] = 'Error al procesar';
            $return['Objeto'] = null;
        }

        return response()->json($return);
    }
}
