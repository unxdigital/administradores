<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AdministratorController extends Controller
{
    public function saveInBatch(Request $request)
    {
        $return['Success'] = false;
        $return['MensajeError'] = 'El archivo json contiene errores';
        $return['Objeto'] = '';

        if ($request->file('file')->isValid() && $request->file('file')->getClientOriginalExtension() === 'json') {
            $dataToSave = $this->getDataFromFile($request->file('file')->path());
            $totalRecords = count($dataToSave);

            if ($totalRecords > 0) {
                DB::table('administrators')->truncate();
                Log::info(sprintf('Tratando de insertar %s registros', $totalRecords));
                $insetData = array_chunk($dataToSave, env('RECORDS_TO_SAVE', 20));
                if (isset($insetData) && !empty($insetData)) {
                    foreach ($insetData as $insert) {
                        Log::info(sprintf('Insertando %s registros', count($insert)));
                        DB::table('administrators')->insert($insert);
                    }

                    $return['Success'] = true;
                    $return['MensajeError'] = 'Archivo Procesado.' . $totalRecords;
                    $return['Objeto'] = $totalRecords;
                }
            }
        } else {
            $return['Success'] = false;
            $return['MensajeError'] = 'Extension no valida solamente archivos json';
            $return['Objeto'] = '';
        }

        return response()->json($return);
    }

    private function getDataFromFile($path) {
        $dataFromFile = json_decode(file_get_contents($path), true);

        $dataToSave = [];
         if (isset($dataFromFile[0]) && isset($dataFromFile[0]['MatriculaId'])) {
             foreach ($dataFromFile as $administrator) {
                 $dataToSave []  = [
                     'MATRICULAID' => isset($administrator['MatriculaId']) ? $administrator['MatriculaId'] : null,
                     'CUIT' => isset($administrator['Cuit']) ? $administrator['Cuit'] : null,
                     'ONEROSO' => isset($administrator['Oneroso']) ? $administrator['Oneroso'] : null,
                     'ESTADOMATRICULAID' => isset($administrator['EstadoMatriculaId']) ? $administrator['EstadoMatriculaId'] : null,
                     'ESTADOMATRICULADESC' => isset($administrator['EstadoMatriculaDesc']) ?$administrator['EstadoMatriculaDesc'] : null,
                     'CANTIDADCONSORCIOS' => isset($administrator['CantidadConsorcios']) ? $administrator['CantidadConsorcios'] : null,
                     'FECHAALTA' => date('Y/m/d H:i:s'),
                     'FECHABAJA' => date('Y/m/d H:i:s'),
                     'FECHASUSPENSION' => date('Y/m/d H:i:s'),
                     'FECHAVENCIMIENTODDJJ' => date('Y/m/d H:i:s'),
                     'FECHAACTUALIZACION' => date('Y/m/d H:i:s'),
                     'TIENESANCIONES' => isset($administrator['TieneSanciones']) ? $administrator['TieneSanciones'] : null,
                     'TIPOPERSONAID' => isset($administrator['TipoPersonaId']) ? $administrator['TipoPersonaId'] : null,
                     'TIPOPERSONADESC' => isset($administrator['TipoPersonaDesc']) ? $administrator['TipoPersonaDesc'] : null,
                     'RAZONSOCIAL' => isset($administrator['RazonSocial']) ? $administrator['RazonSocial'] : null,
                     'TIPODOCUMENTOID' => isset($administrator['TipoDocumentoId']) ? $administrator['TipoDocumentoId'] : null,
                     'TIPODOCUMENTODESC' => isset($administrator['TipoDocumentoDesc']) ? $administrator['TipoDocumentoDesc'] : null,
                     'NRODOCUMENTO' => isset($administrator['NroDocumento']) ? $administrator['NroDocumento'] : null,
                     'SEXO' => isset($administrator['Sexo']) ? $administrator['Sexo'] : null,
                     'CALLE' => isset($administrator['Calle']) ? $administrator['Calle'] : null,
                     'ALTURA' => isset($administrator['Altura']) ? $administrator['Altura'] : null,
                     'PISO' => isset($administrator['Piso']) ? $administrator['Piso'] : null,
                     'DEPARTAMENTO' => isset($administrator['Departamento']) ? $administrator['Departamento'] : null,
                     'CP' => isset($administrator['CP']) ? $administrator['CP'] : null,
                     'COMUNAUSIG' => isset($administrator['ComunaUSIG']) ? $administrator['ComunaUSIG'] : null,
                     'BARRIOUSIG' => isset($administrator['BarrioUSIG']) ? $administrator['BarrioUSIG'] : null,
                     'PROVINCIAID' => isset($administrator['ProvinciaId']) ? $administrator['ProvinciaId'] : null,
                     'TELEFONO' => isset($administrator['Telefono']) ? $administrator['Telefono'] : null,
                     'CELULAR' => isset($administrator['Celular']) ? $administrator['Celular'] : null,
                     'CORREOELECTRONICO' => isset($administrator['CorreoElectronico']) ?$administrator['CorreoElectronico'] : null,
                     'CUITCONSORCIO' => isset($administrator['CuitConsorcio']) ? $administrator['CuitConsorcio'] : null,
                     'CONSORCIOID' => isset($administrator['ConsorcioId']) ? $administrator['ConsorcioId'] : null,
                     'CALLEADMINISTRADOR' => isset($administrator['CalleAdministrador']) ? $administrator['CalleAdministrador'] : null,
                     'ALTURAADMINISTRADOR' => isset($administrator['AlturaAdministrador']) ? $administrator['AlturaAdministrador'] : null,
                     'PISOADMINISTRADOR' => isset($administrator['PisoAdministrador']) ? $administrator['PisoAdministrador'] : null,
                     'DEPARTAMENTOADMINISTRADOR' => isset($administrator['DepartamentoAdministrador']) ? $administrator['DepartamentoAdministrador'] : null,
                     'CPADMINISTRADOR' => isset($administrator['CPAdministrador']) ? $administrator['CPAdministrador'] : null,
                     'COMUNAUSIGADMINISTRADOR' => isset($administrator['ComunaUSIGAdministrador']) ? $administrator['ComunaUSIGAdministrador'] : null,
                     'BARRIOUSIGADMINISTRADOR' => isset($administrator['BarrioUSIGAdministrador']) ? $administrator['BarrioUSIGAdministrador'] : null,
                     'PROVINCIAIDADMINISTRADOR' => isset($administrator['ProvinciaIdAdministrador']) ? $administrator['ProvinciaIdAdministrador'] : null,
                     'MENSAJEID1' => isset($administrator['MensajeId1']) ? $administrator['MensajeId1'] : null,
                     'MENSAJE1' => isset($administrator['Mensaje1']) ? $administrator['Mensaje1'] : null,
                     'MENSAJEID2' => isset($administrator['MensajeId2']) ? $administrator['MensajeId2'] : null,
                     'MENSAJE2' => isset($administrator['Mensaje2']) ? $administrator['Mensaje2'] : null
                 ];

             }
         }

         return $dataToSave;
    }
}
