<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Administrator extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'MATRICULAID',
        'CUIT',
        'ONEROSO',
        'ESTADOMATRICULAID',
        'ESTADOMATRICULADESC',
        'CANTIDADCONSORCIOS',
        'FECHAALTA',
        'FECHABAJA',
        'FECHASUSPENSION',
        'FECHAVENCIMIENTODDJJ',
        'FECHAACTUALIZACION',
        'TIENESANCIONES',
        'TIPOPERSONAID',
        'TIPOPERSONADESC',
        'RAZONSOCIAL',
        'TIPODOCUMENTOID',
        'TIPODOCUMENTODESC',
        'NRODOCUMENTO',
        'SEXO',
        'CALLE',
        'ALTURA',
        'PISO',
        'DEPARTAMENTO',
        'CP',
        'COMUNAUSIG',
        'BARRIOUSIG',
        'PROVINCIAID',
        'TELEFONO',
        'CELULAR',
        'CORREOELECTRONICO',
        'CUITCONSORCIO',
        'CONSORCIOID',
        'CALLEADMINISTRADOR',
        'ALTURAADMINISTRADOR',
        'PISOADMINISTRADOR',
        'DEPARTAMENTOADMINISTRADOR',
        'CPADMINISTRADOR',
        'COMUNAUSIGADMINISTRADOR',
        'BARRIOUSIGADMINISTRADOR',
        'PROVINCIAIDADMINISTRADOR',
        'MENSAJEID1',
        'MENSAJE1',
        'MENSAJEID2',
        'MENSAJE2'
    ];

    /**
     * @param $query
     * @param $value
     */
    public function scopeCuit($query, $value)
    {
        if (!empty(trim($value))) {
            $value = str_replace('-', '', $value);
            $query->where('CUIT', $value);
        }
    }

    /**
     * @param $query
     * @param $value
     */
    public function scopeMatricula($query, $value)
    {
        if (!empty(trim($value))) {
            $query->where('MATRICULAID', $value);
        }
    }

    /**
     * @param $query
     * @param $value
     */
    public function scopeTipoPersona($query, $value)
    {
        if (!empty(trim($value))) {
            $query->where('TIPOPERSONAID', $value);
        }
    }

    /**
     * @param $query
     * @param $value
     */
    public function scopeRazonSocial($query, $value)
    {
        if (!empty(trim($value))) {
            $query->where('RAZONSOCIAL', 'LIKE', "%$value%");
        }
    }

    /**
     * @param $query
     * @param $value
     */
    public function scopeNombre($query, $value)
    {
        if (!empty(trim($value))) {
            $query->where('RAZONSOCIAL', 'LIKE', "%$value%");
        }
    }

    /**
     * @param $query
     * @param $value
     */
    public function scopeApellido($query, $value)
    {
        if (!empty(trim($value))) {
            $query->where('RAZONSOCIAL', 'LIKE', "%$value%");
        }
    }

    /**
     * @param $query
     * @param $value
     */
    public function scopeCalle($query, $value)
    {
        if (!empty(trim($value))) {
            $query->where('CALLE', 'LIKE', "%$value%");
        }
    }

    /**
     * @param $query
     * @param $value
     */
    public function scopeAltura($query, $value)
    {
        if (!empty(trim($value))) {
            $query->where('ALTURA', $value);
        }
    }

    /**
     * @param $query
     * @param $value
     */
    public function scopeCuitConsorcio($query, $value)
    {
        if (!empty(trim($value))) {
            $value = str_replace('-', '', $value);
            $query->where('CUITCONSORCIO', $value);
        }
    }

}
